#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include "organ.h"
#include "organ_internals.h"
#include "scheduler.h"
#include "engine.h"
#include "taps.h"

#define MIN(a,b) (((a)<(b))?(a):(b))

const int cross_talk_levels[] = {
     /* power of two */
     -6,-7,-7
};


/* octave cross talk (offset to wheel number) */
const int _cross_talks[8][3] ={
     {+4,+1,+5},
     {+4,-1,+3},
     {+4,+1,+5},
     {+4,-1,+3},
     {+4,+3,-3},
     {-4,-1,-5},
     {+4,+3,-3},
     {-4,-1,-5}};

struct bus {
     float                 *input_p;
     /* a value to monitor changes */
     float                  value;
};


/* key */

#define KEY_DOWN 0
#define KEY_UP   1

struct manual {
     float              *audio_out;               /* output port */
     oscbuf_t           wheel_amps;
     float              *amp;                     /* per group volume port */
     float              _amp;                     /* low pass amp changes */
};

typedef enum {
     EG_ATTACK,
     EG_SUSTAIN,
     EG_RELEASE,
     EG_OFF
} eg_status_t;

typedef struct eg {
     eg_status_t   status;
     float         value;
     const tap_t  *tap;
     struct eg    *next;
} eg_t;


struct organ {
     struct bus                  buses[NBUSES];
     struct manual               manuals[NMANUALS + 1];

     /* key_pressed has a  (NKEYS + 1) point */
     uint8_t                     key_pressed[NKEYS + 1];

     /* engine is responsible for getting signal out of wheels. It is
        meant for trying different engines. It may be removed in the
        future, once the best engine has been found. In particular, fft
        engines may produce more organic sounds, more resembling the
        natural wheel noise. */
     engine_t                   *engine;

     /* this is where the result of engine process function is put*/
     oscbuf_t                   wheel_buf;

     /* this buffer is used when the ports are unbound */
     float                      _buffer[1024];

     double                      sample_rate;
     /* event scheduler. introduced in particular to implement click
        noise, that is randomly delayed bus activation of a wheel when
        a key is pressed. */
     scheduler_t                scheduler;

     uint8_t                    grand_nnotes;

     float                      perc_amp;

     /* index of perc_bus. this can be changed to switch from 2nd or
        3rd harmonic */

     float                      *perc_switch;
     /* seconds to half the perc value */
     float                      *perc_decay;

     /* amount of cross talk to apply */
     float                      *cross_talk_amount;
     float                      *attack_rate;
     float                      *release_rate;

     float *jitter;
     /* envelope generators; there is one for every tap */
     eg_t *active_egs;          /* stack of active generators */
     eg_t eg[NTAPS];            /* all generators */
     bool touched;              /* flag that implies recalculation of
                                 * generators */
};

/* events */

typedef enum {
     TAP_INC,
     TAP_DEC,
     PERC_ON,
     DRAWBAR
} event_type_t;

typedef struct {
     const uint8_t bus;
     const uint8_t value;
} drawbar_setting_t;

static uint8_t
midi_to_key(const int channel, const int note) {
     switch(channel) {
     case GRAND:
          if (note >= 36 && note < 97) {
               return note - 36;
          }
          break;
     case SWELL:
          if (note >= 36 && note < 97) {
               return note + 25;
          }
          break;
     case PEDAL:
          if (note >= 36 && note < 54) {
               return note + 86;
          }
          break;
     default:
          return NKEYS;
     }
     return NKEYS;
}



organ_t *
organ_new(double sample_rate) {
     organ_t *organ = calloc(1, sizeof(organ_t));
     organ->sample_rate  = sample_rate;
     organ->engine = engine_new(sample_rate);

     /* init scheduler */
     scheduler_init(&organ->scheduler);

     /* init buses */
     for (int bus = 0; bus < NBUSES ; bus++) {
          organ->buses[bus].value = 0.0f;
     }

     /* init perc stops (inner, not bound by the host)*/
     organ->buses[PERC_4].input_p = &(organ->_buffer[0]);
     organ->buses[PERC_2_2_3].input_p = &(organ->_buffer[1]);

     /* init key pressed */
     for (int i = 0; i < NKEYS; i++) {
          organ->key_pressed[i] = KEY_UP;
     }

     /* initialize envelope generators */
     for (int i = 0; i < NTAPS; i++) {
          organ->eg[i].status = EG_OFF;
          organ->eg[i].tap = &taps[i];
          organ->eg[i].next = NULL;
          organ->eg[i].value = 0.0f;
     }
     organ->active_egs = NULL;

     organ->grand_nnotes = 0;
     organ->touched = 0;

     return organ;
}

static uint32_t
click_jitter(const float sample_rate, const float max_ms) {
     const uint32_t jitter = drand48() * max_ms * sample_rate * 1e-3f;
     return jitter;
}

void
organ_drawbar_change(organ_t *organ, const uint8_t bus, const uint8_t value, const uint32_t frame) {
     const drawbar_setting_t db = { .bus = bus, .value = value};
     scheduler_insert_event(&organ->scheduler,
                            frame,
                            DRAWBAR,
                            (char *)&db, sizeof(drawbar_setting_t));
}


static inline void
key2tap(const int key, uint16_t *tap_start, uint16_t *tap_end) {
     if (key < 61) {
          *tap_start = 11 * key;
          *tap_end = *tap_start + 11;

     } else if (key < 121) {
          *tap_start = (key - 61) * 9 + 61 * 11;
          *tap_end = *tap_start + 9;
     } else {
          *tap_start = (key - 122) * 3 + 61 * 11 + 61 * 9;
          *tap_end = *tap_start + 3;
     }
}

void
organ_key_on(organ_t *organ, const uint8_t channel, const uint8_t key, const uint32_t frame) {
     const int key_idx = midi_to_key(channel, key);
     if (key_idx == NKEYS) {
          return;
     }
     uint16_t tap_start, tap_end;
     switch (organ->key_pressed[key_idx]) {
     case KEY_DOWN:
          break;
     case KEY_UP:
          organ->key_pressed[key_idx] = KEY_DOWN;
          key2tap(key_idx, &tap_start, &tap_end);
          assert(tap_start > 0);
          assert(tap_end > tap_start);


          for (uint16_t id = tap_start; id < tap_end; id++) {
               const uint32_t jitter = click_jitter(organ->sample_rate, *(organ->jitter));
               scheduler_insert_event(&organ->scheduler,
                                      jitter + frame,
                                      TAP_INC,
                                      (char *)&id,
                                      sizeof(uint16_t));
          }

          if (key2manual[key_idx] == GRAND) {
               scheduler_insert_event(&organ->scheduler, frame, PERC_ON, NULL, 0);
               /* if (organ->grand_nnotes == 0) { */
               /*      scheduler_insert_event(&organ->scheduler, frame, PERC_ON, NULL, 0); */
               /* } */
               organ->grand_nnotes++;
          }
     }
}


void
organ_key_off(organ_t *organ, const uint8_t channel, const uint8_t key, const uint32_t frame) {
     const int key_idx = midi_to_key(channel, key);
     if (key_idx == NKEYS) {
          return;
     }
     uint16_t tap_start, tap_end;
     key2tap(key_idx, &tap_start, &tap_end);
     assert(tap_start > 0);
     assert(tap_end > tap_start);

     switch (organ->key_pressed[key_idx]) {
     case KEY_UP:
          break;
     case KEY_DOWN:
          organ->key_pressed[key_idx] = KEY_UP;
          for (uint16_t id = tap_start; id < tap_end; id++) {
               uint32_t jitter = click_jitter(organ->sample_rate, *(organ->jitter));

               scheduler_insert_event(&organ->scheduler,
                                      jitter + frame,
                                      TAP_DEC,
                                      (char *)&id,
                                      sizeof(uint16_t));
          }

          if (key2manual[key_idx] == GRAND) {
               organ->grand_nnotes--;
          }
     }
}

#define MIDI_COMMANDMASK 0xF0
#define MIDI_CHANNELMASK 0x0F
#define MIDI_NOTEON  0x90
#define MIDI_NOTEOFF 0x80
#define MIDI_CONTROL 0xB0

void
organ_handle_midi_raw(organ_t *organ, const uint8_t *data, const uint32_t frame) {
     /* assumes three data bytes in data */
     const uint8_t channel = data[0] & MIDI_CHANNELMASK;
     if (channel <= 2) {
          switch (data[0] & MIDI_COMMANDMASK) {
          case MIDI_NOTEON:
               if (data[2] == 0) {      /* NOTE OFF */
                    organ_key_off(organ, channel, data[1], frame);
               } else {
                    /* NOTE ON */
                    organ_key_on(organ, channel, data[1], frame);
               }
               break;
          case MIDI_NOTEOFF:
               organ_key_off(organ, channel, data[1], frame);
               break;
          case MIDI_CONTROL:
               break;
          }
     }
}

static inline void
fill_amps(float* wheel_amps,
          const uint8_t wheel,
          const float amp,
          const float cross_talk_amount) {
     const int octave = wheel / 12;
     wheel_amps[wheel] += amp;
     for (int i = 0; i < 3; i ++) {
          const int offset = _cross_talks[octave][i];
          const int cross_talk_wheel = wheel + offset;
          const int cross_talk_level = cross_talk_levels[i];
          wheel_amps[cross_talk_wheel] += ldexpf(amp, cross_talk_level) * cross_talk_amount;
     }
}

static inline void
eg_calc(struct organ *organ) {
     eg_t *eg = organ->active_egs;
     eg_t newlist = {.next = NULL};
     eg_t *nl = &newlist;     
     const float attack_increment = (organ->attack_rate[0] > 1e-4f ? 1.0f/(organ->attack_rate[0] * organ->sample_rate) : 1.0f);
     const float release_decay = (organ->release_rate[0] > 1e-4f ? 1.0f/(organ->release_rate[0] * organ->sample_rate) : 1.0f);
     const float cross_talk_amount = *organ->cross_talk_amount;
     /* sum all the active envelopes amplitudes into their places
      * while keeping a list of active envelopes */
     organ->touched = 0;
     while (eg) {
          const uint8_t bus   = eg->tap->bus;
          const uint8_t wheel = eg->tap->wheel;
          const uint8_t manual = bus2manual[bus];
          const float bus_value  = organ->buses[bus].value == 0 ? 0.0f : ldexpf(1.0f, organ->buses[bus].value - 8.0f);
          float *amps = organ->manuals[manual].wheel_amps;

          switch (eg->status) {
          case EG_ATTACK:
               eg->value += attack_increment;
               organ->touched |= 1;
               if (eg->value >= 1.0f) {
                    eg->status = EG_SUSTAIN;
                    eg->value = 1.0f;
               }
               fill_amps(amps, wheel, eg->value * bus_value, cross_talk_amount);
               nl->next = eg;
               nl = nl->next;
               break;

          case EG_RELEASE:
               organ->touched |= 1;
               eg->value -= release_decay;
               if (eg->value <= 0.0f) {
                 eg->status = EG_OFF;
                 eg->value = 0.0f;
               }
               fill_amps(amps, wheel, eg->value * bus_value, cross_talk_amount);
               nl->next = eg;
               nl = nl->next;
               break;

          case EG_SUSTAIN:
               fill_amps(amps, wheel, eg->value * bus_value, cross_talk_amount);
               nl->next = eg;
               nl = nl->next;
               break;

          default:
               break;
          }
          eg = eg->next;
     }
     nl->next = NULL;
     organ->active_egs = newlist.next;
}

static void
trigger_percussion(organ_t *organ) {
     organ->perc_amp = 1.0f;
}

static uint32_t
organ_handle_internal_events(organ_t *organ, uint32_t nframes) {
     /* returns MIN of the number of frames till next event or
      * nframes */
#define scheduler_next_frame( s) ((s)->queue->delta)
#define scheduler_has_events( s) ((s)->queue != NULL)

     event_t *evt;
     uint32_t frames_till_next_event;

     /* pops and process all the events at current frame */
     while ((evt = scheduler_get_event(&organ->scheduler))) {
       const event_type_t etype = (event_type_t)evt->type;

          switch (etype) {
          case TAP_INC:
          {
               const uint16_t eg_index = *((const uint16_t *) evt->body);
               eg_t *eg = &organ->eg[eg_index];

               switch (eg->status) {
               case EG_OFF:
                    eg->status = EG_ATTACK;
                    eg->next = organ->active_egs;
                    organ->active_egs = eg;
                    organ->touched |= 1;
                    break;

               case EG_RELEASE:
                    eg->status = EG_ATTACK;
                    break;

               case EG_SUSTAIN:
               case EG_ATTACK:
               default:
                    break;
               }
          }
          break;

          case TAP_DEC:
          {
               const uint16_t eg_index = *((const uint16_t *) evt->body);
               eg_t *eg = &organ->eg[eg_index];

               switch (eg->status) {
               case EG_ATTACK:
               case EG_SUSTAIN:
                    eg->status = EG_RELEASE;
                    organ->touched |= 1;
                    break;
               case EG_OFF:
               case EG_RELEASE:
               default:
                    break;
               }
          }
          break;

          case PERC_ON:
               trigger_percussion(organ);
               break;

          case DRAWBAR:
          {
               const uint8_t value = ((const drawbar_setting_t *) evt->body)->value;
               const uint8_t bus   = ((const drawbar_setting_t *) evt->body)->bus;
               organ->buses[bus].input_p[0] = (float)value;
               organ->touched |= 1;
          }   break;

          default:
               break;
          }
     }

     /* update scheduler */
     if (!scheduler_has_events(&organ->scheduler))
          frames_till_next_event = nframes;
     else {
          frames_till_next_event = MIN(scheduler_next_frame(&organ->scheduler), nframes);
          scheduler_advance_frame(&organ->scheduler, frames_till_next_event);
     }

#undef scheduler_next_frame
#undef scheduler_has_events
     return frames_till_next_event;
}


static inline float
manual_tick(organ_t *o, const uint8_t m_index, oscbuf_t wheels) {
     struct manual *m = &o->manuals[m_index];
     /* if (m->taps_on) { */
     /* manual_calc(o, m_index); */
     /* lowpass volume  (to avoid zipper noise) */
     m->_amp = .02f * m->amp[0] + .98f * m->_amp;
     const float manual_volume = m->_amp;
     float sig = 0.0f;
     for (int w_index = 0; w_index < NOSCS; w_index++) {
       /* scalar product */
          const float amp = m->wheel_amps[w_index] ;
          /* const float amp = (1.0f - 1.0f/powf(1.0f + m->wheel_amps[w_index], .2f)); */
          sig += wheels[w_index] * amp;
     }
     return sig * manual_volume;
}


static float
perc_tick(organ_t *organ) {
#define ln2 0.6931471805599453
     /* do percussion */
     float perc_amp_decay = 1.0f- ln2 / (*(organ->perc_decay) *  organ->sample_rate);

     if (organ->perc_amp > 1e-8f) {
          float sig = manual_tick(organ, PERC, organ->wheel_buf) * organ->perc_amp;
          organ->perc_amp *= perc_amp_decay;
          return sig;
     } else {
          organ->perc_amp = 0.0f;
     }
     return 0.0f;
}

void
organ_process(organ_t *organ, uint32_t nsamples) {
     uint32_t frame = 0;

     /* update percussion buses based on perc_switch port */
     if (organ->perc_switch[0] < 3.5f) {
          organ->buses[PERC_4].input_p[0] = 8.0f;
          organ->buses[PERC_2_2_3].input_p[0] = 0.0f;
     } else {
          organ->buses[PERC_4].input_p[0] = 0.0f;
          organ->buses[PERC_2_2_3].input_p[0] = 8.0f;
     }

     for (int b = 0; b < NBUSES; b++) {
          struct bus *bus = &organ->buses[b];
          if (*bus->input_p != bus->value) {
               bus->value = *bus->input_p;
               organ->touched |= 1;
          }
     }

     while (frame < nsamples) {
          uint32_t frames_left = organ_handle_internal_events(organ, nsamples - frame);
          while (frames_left--) {
               if (organ->active_egs || organ->touched) {
                    engine_tick(organ->engine, organ->wheel_buf);

                    if (organ->touched) {
                         memset(organ->manuals[GRAND].wheel_amps, 0, sizeof(oscbuf_t));
                         memset(organ->manuals[SWELL].wheel_amps, 0, sizeof(oscbuf_t));
                         memset(organ->manuals[PEDAL].wheel_amps, 0, sizeof(oscbuf_t));
                         memset(organ->manuals[PERC].wheel_amps, 0, sizeof(oscbuf_t));
                         eg_calc(organ);
                    }
                    organ->manuals[GRAND].audio_out[frame] = manual_tick(organ,GRAND,organ->wheel_buf);
                    organ->manuals[SWELL].audio_out[frame] = manual_tick(organ,SWELL,organ->wheel_buf);
                    organ->manuals[PEDAL].audio_out[frame] = manual_tick(organ,PEDAL,organ->wheel_buf);
                    organ->manuals[PERC].audio_out[frame] = perc_tick(organ);
               } else {
                    organ->manuals[GRAND].audio_out[frame] = 0.0f;
                    organ->manuals[SWELL].audio_out[frame] = 0.0f;
                    organ->manuals[PEDAL].audio_out[frame] = 0.0f;
                    organ->manuals[PERC].audio_out[frame] =  0.0f;
               }
               frame++;
          }
     }
}

const int
organ_bus_value(const organ_t *organ, const uint8_t b_index) {
     return (int) organ->buses[b_index].value;
}



void
organ_connect_port(organ_t *organ, void *data, const int port) {
     if (port < PERC_4 && port >= 0) {
          organ->buses[port].input_p = data;
     }
     else {
          switch(port) {

          case PORT_GRAND_AMP:
               organ->manuals[GRAND].amp = data;
               break;

          case PORT_SWELL_AMP:
               organ->manuals[SWELL].amp = data;
               break;

          case PORT_PEDAL_AMP:
               organ->manuals[PEDAL].amp = data;
               break;

          case PORT_PERC_AMP:
               organ->manuals[PERC].amp = data;
               break;

          case PORT_PERC_DECAY:
               organ->perc_decay = data;
               break;

          case PORT_PERC_BUS:
               organ->perc_switch = data;
               break;

          case PORT_JITTER:
               organ->jitter = data;
               break;

          case PORT_CROSS_TALK:
               organ->cross_talk_amount = data;
               break;

          case PORT_ATTACK:
               organ->attack_rate = data;
               break;

          case PORT_RELEASE:
               organ->release_rate = data;
               break;
               
          case PORT_GRAND:
               organ->manuals[GRAND].audio_out = data;
               break;

          case PORT_SWELL:
               organ->manuals[SWELL].audio_out = data;
               break;

          case PORT_PEDAL:
               organ->manuals[PEDAL].audio_out = data;
               break;

          case PORT_PERC:
               organ->manuals[PERC].audio_out = data;
               break;
          }
     }
}

void
organ_free(organ_t *o) {
     engine_free(o->engine);
     free(o);
}

BUNDLE = oscar.lv2
INSTALL_DIR = /usr/lib/lv2
DEBUG_CFLAGS= -O0 -ggdb
CFLAGS=-std=gnu99 -fexcess-precision=fast -Wall -O3 -march=native -DNDEBUG
#CFLAGS=-std=gnu99 -fexcess-precision=fast -Wall -O0 -ggdb

$(BUNDLE): manifest.ttl lv2_organ.so Oscar.ttl presets.ttl
	rm -rf $(BUNDLE)
	mkdir $(BUNDLE)
	cp manifest.ttl lv2_organ.so  Oscar.ttl presets.ttl  $(BUNDLE)

%.o: %.c
	gcc -fPIC $(CFLAGS)  -c $< -o $@


lv2_organ.so: organ.o scheduler.o engine.o organ_lv2.o
	gcc -shared  organ.o engine.o scheduler.o organ_lv2.o -o lv2_organ.so


install: $(BUNDLE)
	rm -rf $(INSTALL_DIR)/$(BUNDLE)
	mkdir -p $(INSTALL_DIR)/$(BUNDLE)
	install -m 755 -D  $(BUNDLE)/* $(INSTALL_DIR)/$(BUNDLE)/


clean:
	rm -rf $(BUNDLE) organ.so
	rm -f *.o

tags:
	etags organ.c organ.h engine.c engine.h


test_engine: tests/engine_test.c engine.c 
	gcc -ggdb -O0 tests/engine_test.c -o tests/engine_test -lm

test_organ: tests/organ_test.c organ.c engine.c scheduler.c
	gcc -ggdb -O0 tests/organ_test.c engine.c scheduler.c -o tests/organ_test -lm

test_scheduler: tests/scheduler_test.c
	gcc -ggdb -O0 tests/scheduler_test.c -o tests/scheduler_test

tests: tests/engine_test
	./tests/engine_test

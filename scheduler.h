#include <stdint.h>
#include <stddef.h>

#define SCHEDULER_MAX_EVENTS 2048
#define EVENT_BODY_MAX_SIZE    8


typedef struct event {
     long     delta;         /* time frame difference from present or previous event */
     char  body[EVENT_BODY_MAX_SIZE];
     int  type;          /* event type */
     struct event  *next;    /* next in scheduler priority queue */
     struct event  *s_next;  /* next in events buffer stack */
/* since an event can not be in the buffer stack and in the priority
   queue at the same time I could actually use the same pointer */
} event_t;


typedef struct scheduler {
     event_t  stack[SCHEDULER_MAX_EVENTS];
     event_t *head;          /* ref and unref operation should use only this position */
     event_t *queue;         /* pointer to queue first event */
} scheduler_t;


extern void scheduler_init(scheduler_t *s);

extern int
scheduler_insert_event(scheduler_t *, const uint32_t event_frame,
                       const int type, const char *body, const size_t body_size);


extern void scheduler_advance_frame(scheduler_t *s, const uint32_t nframes);

/* destructively return the first event on the queue */
extern event_t *scheduler_get_event(scheduler_t *s);

#include "organ.h"
#include <stdlib.h>
#include <lv2.h>
#include <lv2/lv2plug.in/ns/ext/urid/urid.h>
#include <lv2/lv2plug.in/ns/ext/atom/atom.h>
#include <lv2/lv2plug.in/ns/ext/atom/util.h>
#include <lv2/lv2plug.in/ns/ext/midi/midi.h>
#include <lv2/lv2plug.in/ns/ext/state/state.h>
#include <lv2/lv2plug.in/ns/ext/patch/patch.h>
#include <math.h>


#define OSCAR       "https://bitbucket.org/sbarbit/oscar"
#define OSCAR_GRAND  OSCAR "#grand"
#define OSCAR_SWELL  OSCAR "#swell"

typedef struct lv2_organ {
     organ_t *organ;
     const LV2_Atom_Sequence		*control_p;
     struct {
          LV2_URID midi_event;
     } uris;
} lv2_organ_t;


static LV2_Handle
instantiate(const struct _LV2_Descriptor * descriptor,
	    double                         sample_rate,
	    const char *                   bundle_path,
	    const LV2_Feature *const *     features)
{
    organ_t* organ = organ_new(sample_rate);
    lv2_organ_t *handle = malloc(sizeof(lv2_organ_t));

    handle->organ = organ;
    
    for (int i = 0; features[i]; ++i) {
	if (!strcmp(features[i]->URI, LV2_URID_MAP_URI)) {
             const LV2_URID_Map *map = features[i]->data;
             handle->uris.midi_event =     map->map(map->handle, LV2_MIDI__MidiEvent);
	}
    }
    /* connect ports to a fake buffer (it should not be required) */
    
    #define MAXBUFSIZE 2048
    float *inner_buffer = malloc(MAXBUFSIZE * 4 * sizeof(float));
    organ_connect_port(organ,inner_buffer,PORT_GRAND);
    organ_connect_port(organ,inner_buffer + 1* MAXBUFSIZE, PORT_SWELL);
    organ_connect_port(organ,inner_buffer + 2* MAXBUFSIZE, PORT_PEDAL);
    organ_connect_port(organ,inner_buffer + 3* MAXBUFSIZE, PORT_PERC);
    return handle;
}



void
lv2_organ_handle_events(lv2_organ_t *m)
{
    LV2_ATOM_SEQUENCE_FOREACH(m->control_p, event) {
	if (event == NULL)
	    continue;
	if (event->body.type == m->uris.midi_event) {
             uint8_t* data = (uint8_t *) LV2_ATOM_BODY(&event->body);
             organ_handle_midi_raw(m->organ, data, event->time.frames);
        }
    }
}


static void
lv2_organ_process(LV2_Handle instance, uint32_t nsamples)
{
    lv2_organ_t *m = (lv2_organ_t *) instance;
    lv2_organ_handle_events(m);
    organ_process (m->organ, nsamples);
}



static void lv2_organ_connect_ports(LV2_Handle instance,
				    uint32_t   port,
				    void *     data_location)
{
    lv2_organ_t *m = (lv2_organ_t *) instance;
    if (port == PORT_CONTROL)
         m->control_p = (const LV2_Atom_Sequence *)data_location;
    else organ_connect_port(m->organ, data_location, port);
}




static void lv2_organ_cleanup(LV2_Handle instance) {
    organ_free(((lv2_organ_t *) instance)->organ);
    free(instance);
}

static const LV2_Descriptor organ = {
    .URI = OSCAR,
    .instantiate = instantiate,
    .connect_port = lv2_organ_connect_ports,
    .activate = NULL,
    .run = lv2_organ_process,
    .deactivate = NULL,
    .cleanup = lv2_organ_cleanup,
    .extension_data = NULL,
};





LV2_SYMBOL_EXPORT const LV2_Descriptor * lv2_descriptor(uint32_t index) {
    switch (index) {
    case 0:
	return &organ;
    default:
	return NULL;
    }
}


#include "organ.h"

#define NGRPS       12
#define NWAVETABLES 8           /* number of wavetables per engine group */
#define NOSCS       NGRPS*NWAVETABLES

typedef float oscbuf_t[NOSCS] __attribute__ ((aligned (32)));

/* ENGINE API */

/* the engine is responsible for producing at each frame an array of
   the current signal of each wheel */

typedef struct engine engine_t;
extern engine_t *engine_new(const float sample_rate);

extern void engine_tick(engine_t *engine, oscbuf_t wheel_buf);
extern void engine_free(engine_t *engine);

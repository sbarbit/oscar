#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <string.h>
#include "engine.h"
#include "organ.h"

     
typedef struct {
     int    table_size;
     int    phi;
     float  *table __attribute__ ((aligned (32)));
} grp_t;

struct engine {
     grp_t      grps[12];
     float 	*mem __attribute__ ((aligned (32)));
};


/* maximum allowed table cum_size */
#define MAX_TABLE_SIZE (1<<15)

static int
cum_table_size(const int nrepeats, const float fund, const float sample_rate ) {
     /* return total table size of a wave teble repeated NREPEATS
      * times at FUND and SAMPLE_RATE */
     return (int) rint(nrepeats * sample_rate/fund);
}

static int
find_optimal_table_repeats(const float freq, const float sample_rate) {
     /* Find the optimal number of repeats of a wavetable, such that
        at the given sample_rate, it produces the desired frequency
        with unitary phase increments. Returns zero if no size is
        found.  */
     double sf = sample_rate/ freq;
     double best_index = 0;
     double best_obj = 1e10;
     double max_iter = MAX_TABLE_SIZE/sf;

     for (double i = 1.0; i< max_iter; i+= 1.0 ) {
	
          double obj = fabs(((sample_rate * i) / rint(i * sf)) - freq);
          if (obj < best_obj) {
               best_obj = obj;
               best_index = i;
          }
     }
     return (uint32_t) best_index;
}

/* qui ogni gruppo ha 8 wavetables per agevolare le istruzioni avx */
void
engine_tick(engine_t *e, oscbuf_t wheel_buf) {
     /* There are 12 groups corresponding to twelve fundamental
      * semitones. Each groups has 8 wavetables, spanning 8
      * octaves. All the wavetables have the same size and are
      * interleaved so that they can be copied in one pass */
     for (int i = 0; i < 12; i++) {
          grp_t *grp = &e->grps[i];
          const int grp_offset = i * 8;
          float *dst = &wheel_buf[grp_offset];
          const float *table = &grp->table[grp->phi];
          memcpy(dst, table, sizeof(float) * 8);
          /* total table length (that is including all the 8
           * wavetables) is obiously a multiple of 8 */
          grp->phi += 8;
          if (grp->phi == grp->table_size) {
               grp->phi = 0;
          }
     }
}


static void
engine_eq(double *wheel_amps,
          const float *wheel_freqs,
          const double dbzero,
          const double dboct,
          const double f0) {
     /* scale amplitude dboct/octave for f > f0, dbzero otherwise  */
     const double R = (dboct/10.0) * log2(10.0f);
     const double A0 = pow(10.0, (dbzero/10.0));
     for (int i = 0; i < NWHEELS; i++) {
          if (wheel_freqs[i] > f0) {
               wheel_amps[i] = pow(wheel_freqs[i]/f0, -R) * A0;
          } else {
               wheel_amps[i] = A0;
          }
     }
}

void
grp_fill_wavetable(grp_t *grp,
                   const int nrepeats) {
     /* the table is filled using interleaved samples from wavetables
      * of 8 octaves */
     const float attenuate = 0.2;
     double initial_phase[8];
     for (int octave = 0; octave < 8; octave++) {
          initial_phase[octave] = M_PI + drand48();
     }
     const int table_size  = grp->table_size / 8;
     float *table = grp->table;
     for (int j = 0; j < table_size; j++) {
          const double phi = j * M_PI * 2.0 * nrepeats/table_size;          
          for (int octave = 0; octave < 8; octave ++ ) {
               const double octave_multiplier = pow(2.0, (double) octave);
               *table++ = attenuate*sin(initial_phase[octave] + phi * octave_multiplier);
          }
     }
}

engine_t *
engine_new(const float sample_rate) {
    int cum_size=0;
    engine_t *e = malloc(sizeof(struct engine));
    int  nrepeats[12];
    int  tablesize[12];
    
    for (int i=0; i< 12; i++) {
         /* the first harmonic (16' register) corresponds to midi note
          * 24 */
         const float fund =  27.5f * pow(2.0f, ((i+3.0f)/12.0f));
         nrepeats[i] = find_optimal_table_repeats(fund, sample_rate);
         tablesize[i] = cum_table_size(nrepeats[i], fund, sample_rate);
         cum_size += tablesize[i] * 8;
    }


    /* allocate a buffer containing all the concatenated sine
       waves */
    e->mem = calloc(cum_size, sizeof(float));

    if (! e->mem) {
	free(e);
	return NULL;
    }

    /* FIXME: reintroduce equalization (or lowpass) */
    /* double wheel_amps[NOSCS]; */
    /* engine_eq(wheel_amps, wheel_freqs, -7, 1, 300); */
    float *table = e->mem;
    
    for (int i=0; i<12; i++) {
         e->grps[i].table_size = tablesize[i] * 8;
         e->grps[i].table = table;
         e->grps[i].phi = 0;
         grp_fill_wavetable(&e->grps[i], nrepeats[i]);
         table += e->grps[i].table_size;
    }
    return e;
}

void engine_free(engine_t *engine) {
    free((void *) engine->mem);
    free(engine);
}

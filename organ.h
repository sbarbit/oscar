#ifndef _ORGAN_H
#define _ORGAN_H
#include <stdint.h>

/* manuals (bus groups) */
#define GRAND       0
#define SWELL       1
#define PEDAL       2
#define PERC        3
#define NMANUALS    4

/* buses */
#define GRAND_16    0
#define GRAND_5_1_3 1
#define GRAND_8     2
#define GRAND_4     3
#define GRAND_2_2_3 4
#define GRAND_2     5
#define GRAND_1_3_5 6
#define GRAND_1_1_3 7
#define GRAND_1     8
#define SWELL_16    9
#define SWELL_5_1_3 10
#define SWELL_8     11
#define SWELL_4     12
#define SWELL_2_2_3 13
#define SWELL_2     14
#define SWELL_1_3_5 15
#define SWELL_1_1_3 16
#define SWELL_1     17
#define PEDAL_16    18
#define PEDAL_5_1_3 19
#define PEDAL_4     20
#define PERC_4      21
#define PERC_2_2_3  22
#define NBUSES      23

#define NKEYS (61 + 61 + 18)
#define NWHEELS 12 * 8

/* list terminators */
#define EOB         NBUSES
#define EOM         NMANUALS
#define EOK         NKEYS


typedef struct organ organ_t;

enum organ_ports {
	PORT_CONTROL  = 21,
        PORT_GRAND_AMP,
        PORT_SWELL_AMP,
        PORT_PEDAL_AMP,
	PORT_PERC_AMP,
	PORT_PERC_DECAY,
	PORT_PERC_BUS,
        PORT_JITTER,
        PORT_CROSS_TALK,
        PORT_ATTACK,
        PORT_RELEASE,
	PORT_GRAND,
	PORT_SWELL,
	PORT_PEDAL,
	PORT_PERC
};


/* events */
extern void organ_handle_midi_raw(organ_t *o, const uint8_t *data, const uint32_t frame);
extern void organ_key_on(organ_t *o, const uint8_t channel, const uint8_t key, const uint32_t frame);
extern void organ_key_off(organ_t *o,const  uint8_t channel,const  uint8_t key, const uint32_t frame);
extern void organ_drawbar_change(organ_t *, const uint8_t bus, const uint8_t value, const uint32_t frame);
extern organ_t *organ_new(const double sample_rate);
extern void organ_free(organ_t *);
extern void organ_process(organ_t *, uint32_t nsamples);
extern void organ_connect_port(organ_t *, void *data, const int port);
extern const int  organ_bus_value(const organ_t *, const uint8_t bus);
extern const uint8_t *bus_groups[];
#endif

#include "scheduler.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

/* A SCHEDULER */
/* handles a the internal event system. It consists of a priority
   queue and a buffer of preallocated events. Event time is expressed
   as frame delta from previous event */

#define scheduler_get_first(s) ((s)->queue)
#define scheduler_set_first(s,e) ({(s)->queue = (e);})

event_t * scheduler_new_event(struct scheduler *s) {
        /* gets an event from events buffer stack */
        event_t *tmp = s->head;
        s->head = tmp->s_next;
        tmp->s_next = NULL;
        return tmp;
}


void scheduler_free_event(struct scheduler *s, event_t *evt) {
        /* puts the event on the head of the buffer stack */
        if (s->head)
                evt->s_next = s->head;
        s->head = evt;
}


void scheduler_init(scheduler_t *s) {
        int i;
        s->queue = NULL;
        for (i = 0; i < SCHEDULER_MAX_EVENTS; i++)
                scheduler_free_event(s, &s->stack[i]);

}

int
scheduler_insert_event(scheduler_t *s, const uint32_t event_frame,
                       const int type,
		       const char *body,
                       const size_t body_size) {

     if (body_size > EVENT_BODY_MAX_SIZE)
          return 0;
     
        /* events are preallocated */
        /* scan the stack of events until it finds the right position */
        event_t *ptr, *first = scheduler_get_first(s), *e = scheduler_new_event(s);
        long frame = 0;
        int pos = 0;

        e->type = type;
        memcpy(e->body, body, body_size);
        
        if (!first) {
                /* event stack is empty */
                scheduler_set_first(s,e);
                e->delta = event_frame;
                assert(!e->next);
                e->next = NULL;
                return pos;
        }

        if (first->delta >= event_frame) {
                /* event is the first of the stack */
                e->next = first;
                first->delta -= event_frame;
                e->delta = event_frame;
                scheduler_set_first(s,e);
                return pos;
        }

        /* linear scan */
        pos++;
        for (ptr = first; ptr; ptr=ptr->next, pos++) {
                frame += ptr->delta;
                if (!ptr->next)
                        break;
                if ((frame + ptr->next->delta) >= event_frame)
                        break;

        }
        assert(pos > 0);
        assert(event_frame >= frame);
        e->delta = event_frame - frame;

        assert(ptr->next != e);
        e->next = ptr->next;
        ptr->next = e;

        if (e->next) {
                e->next->delta += frame - event_frame;
                assert(e->next->delta >= 0);
        }

        return pos;
}



void
scheduler_advance_frame(scheduler_t *s, const uint32_t nframes) {
        /* advance scheduler of nframes frames */
        event_t *first = scheduler_get_first(s);
        if (first) {
                /* assert(first->delta >= nframes); */
                if (first->delta > nframes)
                        first->delta-= nframes;
                else
                        first->delta = 0;
        }
}


/* this should renamed scheduler_pop_event */
event_t *scheduler_get_event(scheduler_t *s) {
        /* destructively return the first event on the queue */
        event_t *evt = scheduler_get_first(s);
        if (evt) {
                if (evt->delta <= 0) {
                        event_t *tmp = evt;
                        assert(evt->delta == 0);
                        scheduler_set_first(s, tmp->next);
                        tmp->next = NULL;
                        scheduler_free_event(s, tmp);
                        return(tmp);
                }
        }
        return NULL;
}
